from bs4 import BeautifulSoup
import requests
import os
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# This function takes url of file and a path where to write it
def file_writer(url, path):
    get_url = requests.get(url)
    content = get_url.content
    name = url.split('/')[-1]
    with open('{}\\{}'.format(path, name), 'wb') as f:
        f.write(content)
        f.close()


# Gets url returns html in text format
def get_html(url):
    get_url = requests.get(url)
    html_text = get_url.text
    return html_text


def get_js_generated_html(url):
    browser = webdriver.PhantomJS()
    browser.get(url)
    last_body_height = 0
    while True:
        body_height = browser.execute_script("return document.body.offsetHeight;")
        if last_body_height != body_height:
            browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            WebDriverWait(browser, 10).until_not(EC.presence_of_all_elements_located((By.ID, "btn_lock")))
            last_body_height = body_height
        else:
            break

    html = browser.page_source
    browser.quit()
    return html


# Gets html and returns all urls and names of albums in a array of tuples
def get_albums(html_text):
    soup = BeautifulSoup(html_text, 'html.parser')
    albums = []
    for link in soup.find_all('a', class_='album_item'):
        album_name = link.find('div', class_='album_name')
        album_name_text = album_name.get_text()
        album_url = link.get('href')
        albums.append((album_url, album_name_text))
    return albums


# Gets album url and path where to save then runs function file_writer to save them on disk
def download_photos(album_url, path):
    album_soup = BeautifulSoup(get_js_generated_html(album_url), 'html.parser')

    for div in album_soup.find_all('div', class_='photos_row'):

        link = div.find('a')
        photo_page = 'https://vk.com{}'.format(link.get('href'))
        image_soup = BeautifulSoup(get_js_generated_html(photo_page), 'html.parser')
        image_div = image_soup.find('div', class_='pv_img_area_wrap')
        image_link = image_div.find('img')
        original_image_url = image_link.get('src')
        file_writer(original_image_url, path)


def main():
    site = 'https://vk.com'
    if len(sys.argv) < 2:
        print('Please specify link to your albums')
        exit()
    my_albums = sys.argv[1] #link to albums

    albums = get_albums(get_html(my_albums))

    for album in albums:
        album_path = './{}'.format(album[1])
        album_url = '{}{}'.format(site, album[0])
        if not os.path.exists(album_path):
            os.makedirs(album_path)
        download_photos(album_url, album_path)


if __name__ == "__main__":
    main()
